# GIT POUR LES MOLDUS

## C'est pourtant pas sorcier !

---

###  CELUI-DONT-ON-NE-DOIT-PAS-PRONONCER-LE-NOM

+++

![git logo](https://s3-us-west-2.amazonaws.com/svgporn.com/logos/git.svg)

+++
> git: a person who is deemed to be despicable or contemptible

+++

![very bad guy](https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-52426.jpg)

+++

### KÉZAKO ?

> git: a fast **revision** control system with a rich **command set** that provides both **high-level operations** and full access to **internals**

+++

git est un système de stockage de données avec des clefs et des valeurs

+++

git est un protocole d'envoi de fichiers


+++

![euh...](https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-260717.png)

---

### POUR COMMENCER

config de git en local

    git config user.name ktulu
    git config user.email ktulu@rly.eh

création d'un dépôt git

    mkdir necrocodicon && cd !$ && git init

+++
créer le répertoire distant **original**

![create a repo on github](https://help.github.com/assets/images/help/repository/repo-create.png)

[https://github.com/new](https://github.com/new)
+++
### LA PETITE COMMISSION

    git add necrocodicon.hpl
    git commit -m "wingardium leviosaaa"

raccourci

    git commit -a -m "wingardium leviosaaa"

+++

### PREMIER ENVOI AU RÉPERTOIRE ORIGINAL

    git remote 
        add origin
            https://github.com/
                ktulu/necrocodicon.git

    git push -u origin master

+++

[what is origin?](https://githowto.com/what_is_origin) [en]

+++

envoi de sortilège

`git push`

+++
questions ?

![git push](https://d.ibtimes.co.uk/en/full/1478857/doctor-strange.jpg)

---

### À L'INTÉRIEUR DE GIT 

qu'y a-t-il dans le dossier ?

    ls .git

branches, objets et références

[doc](https://git-scm.com/book/fr/v2/Les-tripes-de-Git-Plomberie-et-porcelaine) [fr]

+++

- **objects** : le contenu de votre base de données
- **refs** : stocke les pointeurs vers les objets *commit* de ces données (branches)
- **HEAD** : pointe sur la branche de travail en cours
- **index** : infos sur la zone d’attente

+++

### LES VERSIONS DE FICHIER
![arbre de commits](https://www.miximum.fr/wp-content/uploads/2013/07/git_graph1.gif)

+++

### LA TÊTE D'ÉCRITURE

![HEAD et master](http://www.miximum.fr/wp-content/uploads/2013/07/git_graph.gif)

+++

### LES ARBRES
![arbres](https://rogerdudler.github.io/git-guide/img/trees.png)

+++

### LES BRANCHES
![branches](http://www.miximum.fr/wp-content/uploads/2013/07/git_create_branch.gif)

+++?image=https://pre00.deviantart.net/d213/th/pre/i/2006/312/c/6/r__yleh_by_marcsimonetti.jpg&size=contain
questions ?
---

### MANIPULER LES VERSIONS 

erreur dans la commission

    git commit -m "wingardium leviosaaa"


correction
    
    git commit
        --amend -m "wingardium levioooosa"


envoi forcé

    git push --force


+++

![workflow](https://marklodato.github.io/visual-git-guide/basic-usage.svg)

`git status`

+++

![workfow shorcuts](https://marklodato.github.io/visual-git-guide/basic-usage-2.svg)

+++
### GIT CHECKOUT

créer une branche

    git branch avadakavadra
travailler sur la branche

    git checkout avadakavadra
raccourci

    git checkout -b avadakavadra


+++

envoyer une branche au répertoire original distant

    git push origin avadakavadra

détruire une branche

    git branch -d avadakavadra

+++ 

sur quel état j'erre ?

    git branch

retourner à une branche

    git checkout master

+++ 

créer une nouvelle branche

depuis une ancienne commission

    git checkout -b shubniggurath v5

+++

### TÊTE DE LECTURE
### DÉTACHÉE D'UNE BRANCHE

    git checkout -b v3

+++
![detached head](http://www.miximum.fr/wp-content/uploads/2013/07/git_detached_head_commit.gif)
+++

### OEIL D'AGAMOTTO

![agamotto eye](https://i.annihil.us/u/prod/marvel//universe3zx/images/b/b2/EyeofAgamotto.jpg)
`git reflog`

+++

supprimer les modif en cours absentes de l'index

    git checkout .

supprimer les modif en cours dans le dossier dagon

    git checkout dagon/

récupérer la version 3 du dossier dagon

    git checkout v3 dagon/

+++ 

### GIT RESET

faire correspondre l'**index** actuel

à la dernière commision

    git reset

faire correspondre l'**index** et le **répertoire local**

à la dernière commision

    git reset --hard

+++

reculer l'index de 3 commissions

    git reset HEAD~3

+++ 

questions ?

![stay zen](https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-475063.jpg)

---

### CONTRIBUER

récupérer un projet (cloner)

    git clone
        https://github.com/
            ktulu/necrocodicon.git

+++

envoyer sa première contribution 

`git push origin`

+++

mettre à jour son dossier local

des dernières modifications

par les autres contributeurs

`git pull`

+++

#### PULL REQUEST 

proposer à l'auteur original

nos modifications

+++

    git request-pull v1.0.0
    https://github.com/dagon/necrocodicon
    dagon

+++

pull request : [manpage](https://www.git-scm.com/docs/git-request-pull)

+++

fusionner une branche avec la branche active

`git merge expelliarmus`

+++

### GÉRER LES CONFLITS

+++
![conflits](https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-55360.jpg)
+++

`git status`

[doc](https://githowto.com/resolving_conflicts) [en]
+++

### SORTILÈGE IMPARDONNABLE

- supprime l'index et les commits
- récupère la dernière version du contenu du répertoire original
- pointe la tête d'écriture sur la dernière version installée


    git fetch origin
    git reset --hard origin/master

---

### LES ALIAS

ajoutez les lignes suivantes à votre **~/.bashrc**

    alias ajout = "git add"

    alias commission = "git commit -m"
    alias amende = "git commit --amend -m"
    alias grokk = "git commit -a -m"

    alias pousse = "git push"
    
+++

clonage de répertoire original sur DD

    alias clone = "git clone"

+++

mise en cache login/mdp durant 10h

    alias encache = "
    git config --global credential.helper
    'cache --timeout 36000'
    "
+++

voir l'historique

    alias agamotto = "git reflog"
+++

suppression

    alias ote = "git rm"
---

### RESSOURCES

`git help <command>`

+++

### [FR]

- [miximum.fr: enfin-comprendre-git](https://www.miximum.fr/blog/enfin-comprendre-git/)
- [yannesposito.com: 2009-11-12-Git-for-n00b](http://yannesposito.com/Scratch/fr/blog/2009-11-12-Git-for-n00b/)
- [nathanaelcherrier.com: git-bisect](https://blog.nathanaelcherrier.com/2017/06/21/git-bisect/)
- [rogerdudler.github.io: git-guide](https://rogerdudler.github.io/git-guide/index.fr.html)
- [visual git guide](https://marklodato.github.io/visual-git-guide/index-fr.html)


+++

### AWESOME GIT [EN]

- [gitready.com](http://gitready.com)
- [git.io/git-tips/tips](https://github.com/git-tips/tips)
- [gitimmersion.com](http://gitimmersion.com/)
- [think-like-a-git.net](http://think-like-a-git.net/)
- [ohshitgit.com](http://www.ohshitgit.com/)
- [visual git guide](https://marklodato.github.io/visual-git-guide/index-en.html)
- [git in practice](https://github.com/skrypte/GitInPractice)

+++

### AIDES-MÉMOIRES [EN]

- [github-git-cheat-sheet.pdf](https://services.github.com/on-demand/downloads/fr/github-git-cheat-sheet.pdf)

- [gist eashish93 3eca6a90fef1ea6e586b7ec211ff72a5](https://gist.github.com/eashish93/3eca6a90fef1ea6e586b7ec211ff72a5)

+++

### SERVICES

- [github](https://github.com)
- [gitlab](https://gitlab.com)
- [framagit](https://framagit.org)
- [bitbucket](https://bitbucket.org)
- [gogs](https://gogs.io)
---

présentation sur 

[http://gitpitch.com/skrypte/git-pour-les-moldus](http://gitpitch.com/skrypte/git-pour-les-moldus)

MERCI :-)



